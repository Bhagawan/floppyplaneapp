package com.example.flappyplane.util

import androidx.annotation.Keep

@Keep
data class FloppyPlaneSplashResponse(val url : String)