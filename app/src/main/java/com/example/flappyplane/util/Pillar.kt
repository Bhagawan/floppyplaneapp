package com.example.flappyplane.util

data class Pillar(var x : Float, val holeY : Int, var passed : Boolean)
