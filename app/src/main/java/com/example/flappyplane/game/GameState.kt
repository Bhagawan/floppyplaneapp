package com.example.flappyplane.game

import com.example.flappyplane.util.Pillar

data class GameState(val planeY: Float, val planeAngle: Float, val currentImpulse : Float, val record: Int, val pillars: List<Pillar>, val gameState: Int)