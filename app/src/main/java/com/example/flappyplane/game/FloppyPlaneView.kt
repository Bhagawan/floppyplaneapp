package com.example.flappyplane.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.flappyplane.R
import com.example.flappyplane.util.Pillar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.cos
import kotlin.math.sin
import kotlin.random.Random

class FloppyPlaneView(context: Context, attributeSet: AttributeSet?) : View(context, attributeSet) {
    private var mWidth = 0
    private var mHeight = 0
    private var planeX = 0.0f
    private var planeY = 0.0f
    private var planeAngle = 0.0f

    private var dY = 0.0f
    private var impulse = 0.0f
    private var pillarWidth = 100.0f
    private var pillarHoleRadius = 50.0f
    private var pillarMinDistance = 50.0f
    private var pillarSpeed = 1.0f

    private var pillars = ArrayList<Pillar>()

    private var record = 0

    private val plane = BitmapFactory.decodeResource(context.resources, R.drawable.plane)
    private val tPillar = BitmapFactory.decodeResource(context.resources, R.drawable.pillar)
    private var bPillar: Bitmap
    private val restart = AppCompatResources.getDrawable(context,R.drawable.ic_baseline_replay)?.toBitmap()

    private var state = INTRO

    companion object {
        const val INTRO = 0
        const val FLYING = 1
        const val FINISH = 2
    }

    init {
        val rotationMatrix = Matrix()
        rotationMatrix.postRotate(180.0f)
        bPillar = Bitmap.createBitmap(tPillar, 0,0,tPillar.width, tPillar.height, rotationMatrix, true )
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            planeY = mHeight / 2.0f
            planeX = mWidth / 5.0f
            impulse = mHeight / 600.0f * 3.0f
            pillarWidth = mWidth / 20.0f
            pillarHoleRadius = plane.height / 2.0f * 2.0f
            pillarMinDistance = plane.width * 1.5f + pillarWidth * 1.5f
            pillarSpeed = mWidth / 600.0f
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            if(state == FLYING) {
                updatePillars()
                updatePlane()
                drawPillars(it)
            }
            drawPlane(it)
            drawBounds(it)
            drawUI(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                when(state) {
                    INTRO -> {

                    }
                    FLYING -> {
                        tap()
                    }
                    FINISH -> {
                        restart()
                    }
                }
                return true
            }
            MotionEvent.ACTION_UP -> {
                when(state) {
                    INTRO -> {
                        restart()
                    }
                    FLYING -> {

                    }
                    FINISH -> {

                    }
                }
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    /// Public

    fun getState() : GameState = GameState(planeY, planeAngle, dY, record, pillars, state)

    fun setGameState(gameState: GameState) {
        planeY = gameState.planeY
        dY = gameState.currentImpulse
        planeAngle = gameState.planeAngle
        record = gameState.record
        pillars.clear()
        pillars.addAll(gameState.pillars)
        state = gameState.gameState
    }

    //// Private

    private fun drawPlane(c: Canvas) {
        val rotationMatrix = Matrix()
        rotationMatrix.preScale(0.5f, 0.5f)
        rotationMatrix.postRotate(planeAngle)
        val rotatedPlane = Bitmap.createBitmap(plane, 0,0,plane.width, plane.height, rotationMatrix, true )
        val p = Paint()
        p.color = Color.WHITE
        val offY = if(planeAngle >= 0) -plane.height / 2.0f else -rotatedPlane.height.toFloat()
        c.drawBitmap(rotatedPlane, planeX, planeY + offY, p)
    }

    private fun drawBounds(c : Canvas) {
        val p = Paint()
        p.color = Color.WHITE
        p.style = Paint.Style.FILL
        c.drawRect(0.0f, 0.0f, mWidth.toFloat(), 10.0f, p)
        c.drawRect(0.0f, mHeight - 10.0f, mWidth.toFloat(), mHeight.toFloat(), p)
    }

    private fun drawPillars(c : Canvas) {
        val p = Paint()
        for(pillar in pillars) {
            c.drawBitmap(tPillar,null, Rect((pillar.x - pillarWidth / 2).toInt(),10,(pillar.x + pillarWidth / 2).toInt(), (pillar.holeY - pillarHoleRadius).toInt()), p)
            c.drawBitmap(bPillar,null, Rect((pillar.x - pillarWidth / 2).toInt(), (pillar.holeY + pillarHoleRadius).toInt(),(pillar.x + pillarWidth / 2).toInt(), mHeight - 10), p)
        }
    }

    private fun drawUI(c: Canvas) {
        val p = Paint()
        p.color = Color.WHITE
        p.strokeWidth = 3.0f
        p.textSize = 60.0f
        p.textAlign = Paint.Align.CENTER

        when(state) {
            INTRO -> {
                p.color = Color.WHITE
                c.drawText("Нажимайте на экран,", mWidth / 2.0f, mHeight / 3.0f * 2.0f, p)
                c.drawText("чтобы поддерживать самолет", mWidth / 2.0f, mHeight / 3.0f * 2.5f , p)
            }
            FLYING -> {
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey_dark, null)
                c.drawRoundRect(mWidth * 0.3f, -50.0f, mWidth * 0.7f, mHeight * 0.1f, 20f, 20f, p)
                p.color = Color.WHITE
                c.drawText("Пройдено: $record", mWidth / 2.0f, mHeight * 0.07f, p)
            }
            FINISH -> {
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey_dark, null)
                c.drawRoundRect(mWidth * 0.15f, mHeight * 0.3f - 100, mWidth * 0.85f, mHeight * 0.4f, 20f, 20f, p)
                val pad = (restart?.width ?: 90) * 0.5f + 5
                c.drawRoundRect(mWidth * 0.5f - pad, mHeight * 0.7f - pad, mWidth * 0.5f + pad, mHeight * 0.7f + pad, 20f, 20f, p)

                p.color = Color.BLACK
                c.drawText("Результат: $record", mWidth * 0.5f, mHeight * 0.3f, p)

                p.style = Paint.Style.STROKE
                p.color = Color.WHITE
                c.drawRoundRect(mWidth * 0.5f - pad + 5, mHeight * 0.7f - pad + 5, mWidth * 0.5f + pad - 5, mHeight * 0.7f + pad - 5, 20f, 20f, p)

                restart?.let { c.drawBitmap(restart, (mWidth - restart.width) * 0.5f, mHeight * 0.7f - restart.height * 0.5f, p) }
            }
        }
    }

    private fun updatePlane() {
        planeY += dY

        dY += impulse / 20.0f
        planeAngle = ((dY / (impulse / 30.0f))).coerceAtLeast(-30.0f).coerceAtMost(30.0f)

        val noseY = planeY - (plane.width / 2.0f) * sin(Math.toRadians((-planeAngle).toDouble())) //- if(planeAngle > 0) plane.height / 2.0f else 0.0f
        val noseX = planeX + (plane.width / 2.0f) * cos(Math.toRadians((-planeAngle).toDouble()))
        if(noseY < 0.0f || noseY >= mHeight || planeY >= mHeight) {
            state = FINISH
        }

        for(pillar in pillars) if(noseX in (pillar.x - pillarWidth / 2)..(pillar.x + pillarWidth / 2)
            || planeX in (pillar.x - pillarWidth / 2)..(pillar.x + pillarWidth / 2)
            || (planeX < (pillar.x - pillarWidth / 2) && noseX > (pillar.x + pillarWidth / 2))) {
            if(intercepts(planeX, planeY - plane.height / 2.0f, noseX.toFloat(), noseY.toFloat() - plane.height / 2.0f,pillar.x - pillarWidth / 2.0f, 0.0f, pillar.x - pillarWidth / 2.0f, pillar.holeY - pillarHoleRadius)
                || intercepts(planeX, planeY, noseX.toFloat(), noseY.toFloat(),pillar.x - pillarWidth / 2.0f, pillar.holeY + pillarHoleRadius, pillar.x - pillarWidth / 2.0f, mHeight.toFloat())) {
                state = FINISH
            }

            if(intercepts(planeX, planeY - plane.height / 4.0f, noseX.toFloat(), noseY.toFloat(), pillar.x - pillarWidth / 2.0f, pillar.holeY + pillarHoleRadius, pillar.x + pillarWidth / 2.0f, pillar.holeY + pillarHoleRadius)
                || intercepts(planeX, planeY - plane.height / 2.0f, noseX.toFloat(), noseY.toFloat() - plane.height / 2.0f, pillar.x - pillarWidth / 2.0f, pillar.holeY - pillarHoleRadius, pillar.x + pillarWidth / 2.0f, pillar.holeY - pillarHoleRadius)) {
                state = FINISH
            }
        }
    }

    private fun updatePillars() {
        if(pillars.isEmpty()) createPillar()
        else if(pillars.last().x + pillarMinDistance < mWidth) {
            if(Random.nextInt(100) < (mWidth - (pillars.last().x + pillarMinDistance)) / (mWidth / 600.0f)) createPillar()
        }
        var n = 0
        while(n < pillars.size) {
            pillars[n].x -= pillarSpeed
            if(pillars[n].x + pillarWidth / 2.0f < planeX && !pillars[n].passed) {
                record++
                pillars[n].passed = true
            }
            if(pillars[n].x < - pillarWidth / 2) {
                pillars.removeAt(n)
                n--
            }
            n++
        }
    }

    private fun createPillar() {
        pillars.add(Pillar(mWidth.toFloat() + pillarWidth / 2, Random.nextInt(10 + pillarHoleRadius.toInt(), mHeight - 10 - pillarHoleRadius.toInt()), false))
    }

    private fun restart() {
        pillars.clear()
        state = FLYING
        planeY = mHeight / 2.0f
        record = 0
        tap()
    }

    private fun tap() {
        dY = -impulse
    }

    private fun intercepts(ax1: Float, ay1: Float, ax2: Float, ay2: Float, bx1: Float, by1: Float, bx2: Float, by2: Float): Boolean {
        val v1 = ((ax2 - ax1) * (ay2 - by1)) - ((ay2 - ay1) * (ax2 - bx1))
        val v2 = ((ax2 - ax1) * (ay2 - by2)) - ((ay2 - ay1) * (ax2 - bx2))
        val v3 = ((bx2 - bx1) * (by2 - ay1)) - ((by2 - by1) * (bx2 - ax1))
        val v4 = ((bx2 - bx1) * (by2 - ay2)) - ((by2 - by1) * (bx2 - ax2))

        return v1 * v2 < 0 && v3 * v4 < 0
    }
}